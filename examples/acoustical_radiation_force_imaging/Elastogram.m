function [Y_Mod] = Elastogram(Reshaped_Disp_Data_1,Sf,Ef,sf1,sf2,sf3,Ne,t,Y1a)

A1a = zeros(Ef,1);
A2a = zeros(Ef,1);
A3a = zeros(Ef,1);
A4a = zeros(Ef,1);
A5a = zeros(Ef,1);
A6a = zeros(Ef,1);
A7a = zeros(Ef,1);
A8a = zeros(Ef,1);
A9a = zeros(Ef,1);
A10a = zeros(Ef,1);
A11a = zeros(Ef,1);
A12a = zeros(Ef,1);
A13a = zeros(Ef,1);


R1 = zeros(size(Reshaped_Disp_Data_1,1),size(Reshaped_Disp_Data_1,2));

for i = 1:size(Reshaped_Disp_Data_1,1)  % Selecting only non zero rows for Young's Modulus calculation

    for j = 1 : size(Reshaped_Disp_Data_1,2)
        if j <= size(Reshaped_Disp_Data_1,2) - Ne     % (- no. of estimates)
        for f = Sf : Ef
        if f~=sf1 && f~=sf2 && f~=sf3  % We are excluding super frames in our estimation.  
            
            A1 = Reshaped_Disp_Data_1(i,j,:,f);
            ild1 = j;
            
            A2 = Reshaped_Disp_Data_1(i,j+1,:,f);
            ild2 = j+1;
            
            A3 = Reshaped_Disp_Data_1(i,j+2,:,f);
            ild3 = j+2;
            
            A4 = Reshaped_Disp_Data_1(i,j+3,:,f);
            ild4 = j+3;
            
            A5 = Reshaped_Disp_Data_1(i,j+4,:,f);
            ild5 = j+4;
            
            A6 = Reshaped_Disp_Data_1(i,j+5,:,f);
            ild6 = j+5;
            
            A7 = Reshaped_Disp_Data_1(i,j+6,:,f);
            ild7 = j+6;
            
            A8 = Reshaped_Disp_Data_1(i,j+7,:,f);
            ild8 = j+7;
            
            A9 = Reshaped_Disp_Data_1(i,j+8,:,f);
            ild9 = j+8;
            
            A10 = Reshaped_Disp_Data_1(i,j+9,:,f);
            ild10 = j+9;
            
            A11 = Reshaped_Disp_Data_1(i,j+10,:,f);
            ild11 = j+10;
            
            A12 = Reshaped_Disp_Data_1(i,j+11,:,f);
            ild12 = j+11;
            
            A13 = Reshaped_Disp_Data_1(i,j+12,:,f);
            ild13 = j+12;
            
            
            A1a(f,:) = A1;
            A2a(f,:) = A2;
            A3a(f,:) = A3;
            A4a(f,:) = A4;
            A5a(f,:) = A5;
            A6a(f,:) = A6;
            A7a(f,:) = A7;
            A8a(f,:) = A8;
            A9a(f,:) = A9;
            A10a(f,:) = A10;
            A11a(f,:) = A11;
            A12a(f,:) = A12;
            A13a(f,:) = A13;
                        
        end
        end
            
            [M1,I1] = max(A1a);
            [M2,I2] = max(A2a);
            [M3,I3] = max(A3a);
            [M4,I4] = max(A4a);
            [M5,I5] = max(A5a);
            [M6,I6] = max(A6a);
            [M7,I7] = max(A7a);
            [M8,I8] = max(A8a);
            [M9,I9] = max(A9a);
            [M10,I10] = max(A10a);
            [M11,I11] = max(A11a);
            [M12,I12] = max(A12a);
            [M13,I13] = max(A13a);
                       
            
            B1(1,:) = t(I1);
            B1(2,:) = t(I2);
            B1(3,:) = t(I3);
            B1(4,:) = t(I4);
            B1(5,:) = t(I5);
            B1(6,:) = t(I6);
            B1(7,:) = t(I7);
            B1(8,:) = t(I8);
            B1(9,:) = t(I9);
            B1(10,:) = t(I10);
            B1(11,:) = t(I11);
            B1(12,:) = t(I12);
            B1(13,:) = t(I13);
                                       

            A_T = [B1(1,:)*10^(-2);
                   B1(2,:)*10^(-2);
                   B1(3,:)*10^(-2);
                   B1(4,:)*10^(-2);
                   B1(5,:)*10^(-2);
                   B1(6,:)*10^(-2);
                   B1(7,:)*10^(-2);
                   B1(8,:)*10^(-2);
                   B1(9,:)*10^(-2);
                   B1(10,:)*10^(-2);
                   B1(11,:)*10^(-2);
                   B1(12,:)*10^(-2);
                   B1(13,:)*10^(-2);
                                  ];


% ----------------------------------->
     
ld1 = Y1a(i,ild1);
ld2 = Y1a(i,ild2);
ld3 = Y1a(i,ild3);
ld4 = Y1a(i,ild4);
ld5 = Y1a(i,ild5);
ld6 = Y1a(i,ild6);
ld7 = Y1a(i,ild7);
ld8 = Y1a(i,ild8);
ld9 = Y1a(i,ild9);
ld10 = Y1a(i,ild10);
ld11 = Y1a(i,ild11);
ld12 = Y1a(i,ild12);
ld13 = Y1a(i,ild13);

    
% ----------------------------------->          

            L_D = [ld1*10^(-3); 
                   ld2*10^(-3); 
                   ld3*10^(-3); 
                   ld4*10^(-3); 
                   ld5*10^(-3); 
                   ld6*10^(-3); 
                   ld7*10^(-3); 
                   ld8*10^(-3); 
                   ld9*10^(-3); 
                   ld10*10^(-3); 
                   ld11*10^(-3); 
                   ld12*10^(-3); 
                   ld13*10^(-3); 
                              ];


X=L_D;

% Adding noise to equation y = 3x + 7
Y=A_T;

% If data is not normalized, Zscore Normalization (Advised for SGD)
% data=[zscore(X),Y];
data=[X,Y];

% Define a symbolic varible for function plot
syms x
%% CV Partition
% * 70% to training set + 30% to testing set 

[train_set,test_set ] = holdout(data,70);

X_train=train_set(:,1:end-1); Y_train=train_set(:,end);
X_test=test_set(:,1:end-1); Y_test=test_set(:,end);

% Number of training instances
N=length(X_train);

% Number of testing instances
M=length(X_test);
%% 1.  Using Direct Method
% * Append a vectors of one to _X_train _for calculating *bias*.
%%
W=pinv([ones(N,1) X_train])*Y_train;
W1=pinv([X_train ones(N,1)])*Y_train;

% y - intercept
y_intercept = W(1);

% m - slope
m = W(2);
C_T = 1/m;
C_T_Sq = C_T^(2);
Mew = C_T_Sq * 1000;
Y = 3*Mew;

R1(i,ild7) = Y;
 
        end
    end
end

figure;
clf;
imagesc(R1);
caxis([0*10^(3) 92*10^3]);
colorbar;
title('Youngs Modulus Image');
Y_Mod = R1;

close(6);

end