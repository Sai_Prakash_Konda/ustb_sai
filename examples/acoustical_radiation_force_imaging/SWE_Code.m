%% Acoustic Radiation Force Imaging from UFF file recorded with the Verasonics ARFI_L7 example
%
% In this example we demonstrate the speckle tracking of displacement created
% by a shear wave induced with an acoustic "push" pulse. Also known as
% acoustic radiation force imaging, or share wave elastography. You will 
% need an internet connection to download the data. Otherwise, you can run 
% the *ARFI_L7.m* Verasonics example and create your own .uff file.
%
% _Ole Marius Hoel Rindal <olemarius@olemarius.net> 15.08.2017_

%% Reading the channel data from the UFF file
clear all; close all;

% data location
url='http://ustb.no/datasets/';            % if not found data will be downloaded from here
local_path = [data_path(), filesep];  % location of example data on this computer

filename='ARFI_dataset.uff';

% check if the file is available in the local path & downloads otherwise
tools.download(filename, url, local_path);

%%
% Reading channel data from the UFF file
% and print out information about the dataset.

channel_data = uff.read_object([local_path filename],'/channel_data');
channel_data.print_authorship

%% Beamform images 
% First, we need to beamform the images from the channel data. We'll do the
% usual drill of defining the scan and the beamformer object.

% SCAN
sca=uff.linear_scan();
sca.x_axis = linspace(channel_data.probe.x(1),channel_data.probe.x(end),256).';
sca.z_axis = linspace(0,30e-3,256).';
 
% Define processing pipeline
pipe=pipeline();
pipe.channel_data=channel_data;
pipe.scan=sca;

pipe.receive_apodization.window=uff.window.tukey50;
pipe.receive_apodization.f_number=1.7;

pipe.transmit_apodization.window=uff.window.tukey50;
pipe.transmit_apodization.f_number=1.7;

% Start the processing pipeline
b_data=pipe.go({midprocess.das postprocess.coherent_compounding});

%% Show beamformed images
% The b-mode images is really not that interesting. The displacement
% created by the shear waves are to small to be seen. Notice that we barely
% see a circular structure at about x = 10 and z = 15 mm. This is a sphere
% in the phantom which is somewhat harder than the surrounding structure.

b_data.plot(1,['B-mode'],[30]);

%% Estimate displacement
% To actually see the shear wave propagation we need to estimate the
% displacement. This is done using a USTB *process* called
% *autocorrelation_displacement_estimation*. Have a look at its references to see
% the details.

disp = postprocess.autocorrelation_displacement_estimation();
disp.channel_data = channel_data;
disp.input = b_data;
disp.z_gate = 4;        % Nbr of samples to average estimate in depth / z
disp.x_gate = 2;        % Nbr of samples to average estimate in lateral / x
disp.packet_size = 2;   % How many frames to use in the estimate
displacement_estimation = disp.go();
disp.print_reference    % This is the references
disp.print_implemented_by    % Credits to the people who implemented it ;)

%%
% We also have an alternative implementation also estimating the center
% frequency. This is the
% *modified_autocorrelation_displacement_estimation*. Please see the
% individiual implementations and references for more details.

disp_mod = postprocess.modified_autocorrelation_displacement_estimation();
disp_mod.channel_data = channel_data;
disp_mod.input = b_data;
disp_mod.z_gate = 4;        % Nbr of samples to average estimate in depth / z
disp_mod.x_gate = 2;        % Nbr of samples to average estimate in lateral / x
disp_mod.packet_size = 2;   % How many frames to use in the estimate
displacement_estimation_modified = disp_mod.go();
disp_mod.print_reference    % This is the references
disp_mod.print_implemented_by    % Credits to the people who implemented it ;)
%% Display the displacement estimate
% Now, we can finally show the estimated displacement. This is nice to
% visualize as a movie.
%
% In the movie we can clearly see the *acoustic radiation force* push
% that creates the share waves. The push is centered at x = 0 mm and 
% z = 14 mm. Notice how the shear waves interacts with the harder
% spherical structure at x = 10 and z = 15 mm. The wavefront is moving
% faster through the harder tissue and some complex wavefronts are created.
%
% Notice that we give the figure and the title as arguments to the plot
% function. The empty brackets [] is because we don't want to specify any
% dynamic range (well do that with the caxis function). And the 'none' is
% because we don't want to do any compression of the displacement data

f2 = figure(2);clf;
displacement_estimation.plot(f2,['Displacement'],[],'none');
caxis([-0.1*10^-6 0.2*10^-6]); % Updating the colorbar
colormap(gca(f2),'hot');       % Changing the colormap

f3 = figure(3);clf;
displacement_estimation_modified.plot(f3,['Displacement modified estimation'],[],'none');
caxis([-0.1*10^-6 0.2*10^-6]); % Updating the colorbar
colormap(gca(f3),'hot');       % Changing the colormap

%%
% Let's check the estimated center frequency and make sure that it is 
% around 5 MHz as it should be. It is, but is the estimate any better?
figure(4);
imagesc(displacement_estimation_modified.scan.x_axis*1000,displacement_estimation_modified.scan.z_axis*1000,disp_mod.estimated_center_frequency(:,:,10));
xlabel('X [mm]');ylabel('Z [mm]');title('Estimated Center Frequency');
colorbar;

%% We can do it all in once!!
% Since the *autocorrelation_displacement_estimation* is a process, we can trust
% the default paramters (which are the same as the ones used above) and do
% the beamforming and the displacement estimation all in one call!
%
% And the pipe will check if some of the calculations allready have been
% done and skip them.
%
% Isn't the USTB great?!

disp = postprocess.autocorrelation_displacement_estimation();
disp.channel_data = channel_data;

disp_img = pipe.go({midprocess.das postprocess.coherent_compounding ...
                                disp});
%%
% Display the displacement 
% Which gives us the same result as above.
f5 = figure(5);clf;
disp_img.plot(f5,['Displacement'],[],'none');
caxis([-0.1*10^-6 0.2*10^-6]); % Updating the colorbar
colormap(gca(f5),'hot');       % Changing the colormap


%% Shear Wave Elastography - Matlab implementation of Shear Wave Elastography
%
% Author: Sai Prakash Reddy Konda (Sai) (reddyprakash092@gmail.com)
%           
% References: 
%
% 1. M. L. Palmeri, M. H. Wang, J. J. Dahl, K. D. Frinkley and K. R. Nightingale, 
% “Quantifying hepatic shear modulus in vivo using acoustic radiation force”. 
% Ultrasound in Medicine and Bio-logy, 34(4):546–558, 2008 
% 
% 2. Sai Konda, Master's Project Report titled,
% “Frequency Domain Beamforming in Acoustical Radiation Force Impulse
% Imaging”, 2020.
%
% Please send me an email, in order to request my project report. For
% better understanding of the code, I would request you to kindly go
% through the references.
%
% $Last updated: 2021/04/08$


Disp_Data = displacement_estimation.data;


% Shear Wave Elastography (or Young's modulus/tissue stiffness) is estimated 
% from displacement data. This displacement data is initially a single column data, 
% which is to be reshaped (into rows, columns and frames), in order to estimate Young's Modulus. 

% Number of rows, columns and frames are hard coded values, since they are data dependent. 
% These values change as per the displacement data (and channel data). So, I would request
% the users of this code to modify these values as per your data. 

% Since super frames do not contain any displacement information, we have to remove super frames
% from displacement data in our Young's Modulus estimation. This will improve accuracy of 
% our Young's Modulus image.

Nr = 256; Nc = 256; Nfd1 = 199; % Number of rows, columns and frames in the displacement data. 
Reshaped_Disp_Data_1 = reshape(Disp_Data,[Nr Nc 1 Nfd1]); % reshaped as per above description
sf1 = 50; sf2 = 100; sf3 = 150; % We are excluding super frames in our estimation.

df1a = 1; df1b = 49; % Displacement frames w.r.t initial push pulse, there are 49 displacement frames.
df2a = 51; df2b = 99; % Displacement frames w.r.t super frame 1, there are 49 displacement frames.
df3a = 101; df3b = 149; % Displacement frames w.r.t super frame 2, there are 49 displacement frames. 
df4a = 151; df4b = 199; % Displacement frames w.r.t super frame 3, there are 49 displacement frames.



Ne = 13; % Ne represents number of estimates for Young's Modulus calculation. [1],[2]

B1 = zeros(Ne,1);

t = 0:0.5/50:2;
t = t(1:199);
t = t'; % t for time of flight calculation, t value denotes the time taken for the ARF impulse. [1],[2]


% ---------------------------------------------- Lateral Distance Calculation for estimating Young's Modulus, [1],[2] ----------------------------------------------->

X1 = sca.x_axis' * 1e3;
Y1a = zeros(size(Reshaped_Disp_Data_1,1),size(Reshaped_Disp_Data_1,2));


for i = 1:(size(Reshaped_Disp_Data_1,1))
    for j = 1:size(Reshaped_Disp_Data_1,2)
        
         h = abs(size(Reshaped_Disp_Data_1,1)/2 + 1 - i);
         

         h1 = X1(1,abs(size(Reshaped_Disp_Data_1,2)/2 + 1 - h));

         Y1a(i,j) = sqrt(h1^2 + X1(1,j)^2);
        
    end
end


% ---------------------------------------------- Lateral Distance Calculation for estimating Young's Modulus, [1],[2] ----------------------------------------------->

% -------------------------------------------------------- Main code for estimating Young's modulus, [1],[2] ---------------------------------------------------------->

% This function 'Elastogram' is the main code, which calculates Shear Wave Elastography 
% (or Young's modulus). It takes displacement data, its related information as input and will produce
% Young's modulus results. [1],[2]

[Y_Mod_1_to_49_01] = Elastogram(Reshaped_Disp_Data_1,df1a,df1b,sf1,sf2,sf3,Ne,t,Y1a);
[Y_Mod_51_to_99_01] = Elastogram(Reshaped_Disp_Data_1,df2a,df2b,sf1,sf2,sf3,Ne,t,Y1a);
[Y_Mod_101_to_149_01] = Elastogram(Reshaped_Disp_Data_1,df3a,df3b,sf1,sf2,sf3,Ne,t,Y1a);
[Y_Mod_151_to_199_01] = Elastogram(Reshaped_Disp_Data_1,df4a,df4b,sf1,sf2,sf3,Ne,t,Y1a);


% -------------------------------------------------------- Main code for estimating Young's modulus, [1],[2] ---------------------------------------------------------->

% ------------------------------------------------------ Averaging Young's modulus results across super frames, [2] ----------------------------------------------------->

Y_Part_01 = 1/4 * (Y_Mod_1_to_49_01 + Y_Mod_51_to_99_01 + Y_Mod_101_to_149_01 + Y_Mod_151_to_199_01);
figure;
imagesc(Y_Part_01);
caxis([0 92*10^(3)]);
title('Average of 4 Groups of Frames, each Group has 49 Frames');

Y_Part_01 = uint16(Y_Part_01);

Y_Part_01_rec = Y_Part_01;

Y_Part_02 = Y_Part_01_rec;

figure;
imagesc(Y_Part_02);
caxis([0 92*10^(3)]);
colorbar;
title('Average across super frames, Y_Part_01');


x1 = sca.x_axis'*1e3;
x1 = x1(:,1:256);


x2 = sca.z_axis'*1e3;
x2 = x2(:,1:256);

% 
figure;
AXX = imagesc( axes, x1, x2, Y_Part_02);
colorbar;
title('Average across super frames, Y_Part_01');
xlabel('x[mm]');
ylabel('z[mm]');

% ------------------------------------------------------ Averaging Young's modulus results across super frames, [2] ----------------------------------------------------->

% --------------------------------------------------------------------------------- Averaging Filter, [2] --------------------------------------------------------------------------------->

size = 21;

I = (Y_Part_02);
 
J1 = fspecial('disk', size);
K1 = imfilter(I,J1,'replicate');

figure;
imagesc(K1);
title('Averaging Filter with Disc Size = 21, Y_Part_01');


figure;
AXX = imagesc( axes, x1, x2, K1);
colorbar;
title('Averaging Filter with Disc Size = 21 Pixels, Final Elastogram Image, Y_Part_01');
xlabel('x[mm]');
ylabel('z[mm]');

% --------------------------------------------------------------------------------- Averaging Filter, [2] --------------------------------------------------------------------------------->

size = 35;

I = (Y_Part_02);
 
J1 = fspecial('disk', size);
K1 = imfilter(I,J1,'replicate');
figure;
imagesc(K1);
title('Averaging Filter with Disc Size = 35, Y_Part_01');


InI = K1;
figure;
AXX = imagesc( axes, x1, x2, K1);
colorbar;
title('Averaging Filter with Disc Size = 35 Pixels, Final Elastogram Image, Y_Part_01');
xlabel('x[mm]');
ylabel('z[mm]');

% --------------------------------------------------------------------------------- Averaging Filter, [2] --------------------------------------------------------------------------------->
